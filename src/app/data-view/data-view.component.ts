import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../data-view.service';
import { Form } from '@angular/forms';


@Component({
  selector: 'app-data-view',
  templateUrl: './data-view.component.html',
  styleUrls: ['./data-view.component.css']
})
export class DataViewComponent {

  @Input() config;
  @Output() submit = new EventEmitter<{name: string, state: string}>();

  selectedValues = {
    name: '',
    state: ''
  };

  constructor() { }

  onFormSubmit(f) {
   this.selectedValues.name = f.value.personname;
   this.selectedValues.state = f.value.states;
   this.submit.emit(this.selectedValues);
  }

}
