import { Component, OnInit } from '@angular/core';
import { DataService } from './data-view.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'tela';
  flexiConfig;
  name;
  state;

  constructor(private ds: DataService) { }

  ngOnInit() {
    this.ds.getData().subscribe( data => {
      this.flexiConfig = data;
      console.log(this.flexiConfig);
});
}

  onSubmit(e) {
    this.name=e.name;
    this.state= e.state;
  }

}